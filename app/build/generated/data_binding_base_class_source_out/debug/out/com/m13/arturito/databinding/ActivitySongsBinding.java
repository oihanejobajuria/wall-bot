// Generated by view binder compiler. Do not edit!
package com.m13.arturito.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.github.mikephil.charting.charts.PieChart;
import com.m13.arturito.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivitySongsBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final TextView all;

  @NonNull
  public final CardView cardView1;

  @NonNull
  public final CardView cardView10;

  @NonNull
  public final CardView cardView2;

  @NonNull
  public final CardView cardView4;

  @NonNull
  public final CardView cardView5;

  @NonNull
  public final CardView cardView7;

  @NonNull
  public final CardView cardView8;

  @NonNull
  public final CardView cardView9;

  @NonNull
  public final TextView dateSongs;

  @NonNull
  public final TextView lessListen;

  @NonNull
  public final LinearLayout linear4;

  @NonNull
  public final LinearLayout linear5;

  @NonNull
  public final LinearLayout linear6;

  @NonNull
  public final LinearLayout linear7;

  @NonNull
  public final TextView mostListen;

  @NonNull
  public final PieChart pie1;

  @NonNull
  public final TextView specs2;

  @NonNull
  public final TextView textView;

  @NonNull
  public final TextView textView10;

  @NonNull
  public final TextView textView12;

  @NonNull
  public final TextView textView14;

  @NonNull
  public final TextView textView20;

  @NonNull
  public final TextView textView21;

  @NonNull
  public final TextView textView24;

  @NonNull
  public final TextView textView25;

  @NonNull
  public final TextView textView27;

  @NonNull
  public final TextView textView28;

  @NonNull
  public final TextView textView29;

  @NonNull
  public final TextView textView7;

  @NonNull
  public final ToolbarListBinding toolbarList;

  private ActivitySongsBinding(@NonNull ConstraintLayout rootView, @NonNull TextView all,
      @NonNull CardView cardView1, @NonNull CardView cardView10, @NonNull CardView cardView2,
      @NonNull CardView cardView4, @NonNull CardView cardView5, @NonNull CardView cardView7,
      @NonNull CardView cardView8, @NonNull CardView cardView9, @NonNull TextView dateSongs,
      @NonNull TextView lessListen, @NonNull LinearLayout linear4, @NonNull LinearLayout linear5,
      @NonNull LinearLayout linear6, @NonNull LinearLayout linear7, @NonNull TextView mostListen,
      @NonNull PieChart pie1, @NonNull TextView specs2, @NonNull TextView textView,
      @NonNull TextView textView10, @NonNull TextView textView12, @NonNull TextView textView14,
      @NonNull TextView textView20, @NonNull TextView textView21, @NonNull TextView textView24,
      @NonNull TextView textView25, @NonNull TextView textView27, @NonNull TextView textView28,
      @NonNull TextView textView29, @NonNull TextView textView7,
      @NonNull ToolbarListBinding toolbarList) {
    this.rootView = rootView;
    this.all = all;
    this.cardView1 = cardView1;
    this.cardView10 = cardView10;
    this.cardView2 = cardView2;
    this.cardView4 = cardView4;
    this.cardView5 = cardView5;
    this.cardView7 = cardView7;
    this.cardView8 = cardView8;
    this.cardView9 = cardView9;
    this.dateSongs = dateSongs;
    this.lessListen = lessListen;
    this.linear4 = linear4;
    this.linear5 = linear5;
    this.linear6 = linear6;
    this.linear7 = linear7;
    this.mostListen = mostListen;
    this.pie1 = pie1;
    this.specs2 = specs2;
    this.textView = textView;
    this.textView10 = textView10;
    this.textView12 = textView12;
    this.textView14 = textView14;
    this.textView20 = textView20;
    this.textView21 = textView21;
    this.textView24 = textView24;
    this.textView25 = textView25;
    this.textView27 = textView27;
    this.textView28 = textView28;
    this.textView29 = textView29;
    this.textView7 = textView7;
    this.toolbarList = toolbarList;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivitySongsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivitySongsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_songs, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivitySongsBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.all;
      TextView all = ViewBindings.findChildViewById(rootView, id);
      if (all == null) {
        break missingId;
      }

      id = R.id.cardView1;
      CardView cardView1 = ViewBindings.findChildViewById(rootView, id);
      if (cardView1 == null) {
        break missingId;
      }

      id = R.id.cardView10;
      CardView cardView10 = ViewBindings.findChildViewById(rootView, id);
      if (cardView10 == null) {
        break missingId;
      }

      id = R.id.cardView2;
      CardView cardView2 = ViewBindings.findChildViewById(rootView, id);
      if (cardView2 == null) {
        break missingId;
      }

      id = R.id.cardView4;
      CardView cardView4 = ViewBindings.findChildViewById(rootView, id);
      if (cardView4 == null) {
        break missingId;
      }

      id = R.id.cardView5;
      CardView cardView5 = ViewBindings.findChildViewById(rootView, id);
      if (cardView5 == null) {
        break missingId;
      }

      id = R.id.cardView7;
      CardView cardView7 = ViewBindings.findChildViewById(rootView, id);
      if (cardView7 == null) {
        break missingId;
      }

      id = R.id.cardView8;
      CardView cardView8 = ViewBindings.findChildViewById(rootView, id);
      if (cardView8 == null) {
        break missingId;
      }

      id = R.id.cardView9;
      CardView cardView9 = ViewBindings.findChildViewById(rootView, id);
      if (cardView9 == null) {
        break missingId;
      }

      id = R.id.dateSongs;
      TextView dateSongs = ViewBindings.findChildViewById(rootView, id);
      if (dateSongs == null) {
        break missingId;
      }

      id = R.id.lessListen;
      TextView lessListen = ViewBindings.findChildViewById(rootView, id);
      if (lessListen == null) {
        break missingId;
      }

      id = R.id.linear4;
      LinearLayout linear4 = ViewBindings.findChildViewById(rootView, id);
      if (linear4 == null) {
        break missingId;
      }

      id = R.id.linear5;
      LinearLayout linear5 = ViewBindings.findChildViewById(rootView, id);
      if (linear5 == null) {
        break missingId;
      }

      id = R.id.linear6;
      LinearLayout linear6 = ViewBindings.findChildViewById(rootView, id);
      if (linear6 == null) {
        break missingId;
      }

      id = R.id.linear7;
      LinearLayout linear7 = ViewBindings.findChildViewById(rootView, id);
      if (linear7 == null) {
        break missingId;
      }

      id = R.id.mostListen;
      TextView mostListen = ViewBindings.findChildViewById(rootView, id);
      if (mostListen == null) {
        break missingId;
      }

      id = R.id.pie1;
      PieChart pie1 = ViewBindings.findChildViewById(rootView, id);
      if (pie1 == null) {
        break missingId;
      }

      id = R.id.specs2;
      TextView specs2 = ViewBindings.findChildViewById(rootView, id);
      if (specs2 == null) {
        break missingId;
      }

      id = R.id.textView;
      TextView textView = ViewBindings.findChildViewById(rootView, id);
      if (textView == null) {
        break missingId;
      }

      id = R.id.textView10;
      TextView textView10 = ViewBindings.findChildViewById(rootView, id);
      if (textView10 == null) {
        break missingId;
      }

      id = R.id.textView12;
      TextView textView12 = ViewBindings.findChildViewById(rootView, id);
      if (textView12 == null) {
        break missingId;
      }

      id = R.id.textView14;
      TextView textView14 = ViewBindings.findChildViewById(rootView, id);
      if (textView14 == null) {
        break missingId;
      }

      id = R.id.textView20;
      TextView textView20 = ViewBindings.findChildViewById(rootView, id);
      if (textView20 == null) {
        break missingId;
      }

      id = R.id.textView21;
      TextView textView21 = ViewBindings.findChildViewById(rootView, id);
      if (textView21 == null) {
        break missingId;
      }

      id = R.id.textView24;
      TextView textView24 = ViewBindings.findChildViewById(rootView, id);
      if (textView24 == null) {
        break missingId;
      }

      id = R.id.textView25;
      TextView textView25 = ViewBindings.findChildViewById(rootView, id);
      if (textView25 == null) {
        break missingId;
      }

      id = R.id.textView27;
      TextView textView27 = ViewBindings.findChildViewById(rootView, id);
      if (textView27 == null) {
        break missingId;
      }

      id = R.id.textView28;
      TextView textView28 = ViewBindings.findChildViewById(rootView, id);
      if (textView28 == null) {
        break missingId;
      }

      id = R.id.textView29;
      TextView textView29 = ViewBindings.findChildViewById(rootView, id);
      if (textView29 == null) {
        break missingId;
      }

      id = R.id.textView7;
      TextView textView7 = ViewBindings.findChildViewById(rootView, id);
      if (textView7 == null) {
        break missingId;
      }

      id = R.id.toolbarList;
      View toolbarList = ViewBindings.findChildViewById(rootView, id);
      if (toolbarList == null) {
        break missingId;
      }
      ToolbarListBinding binding_toolbarList = ToolbarListBinding.bind(toolbarList);

      return new ActivitySongsBinding((ConstraintLayout) rootView, all, cardView1, cardView10,
          cardView2, cardView4, cardView5, cardView7, cardView8, cardView9, dateSongs, lessListen,
          linear4, linear5, linear6, linear7, mostListen, pie1, specs2, textView, textView10,
          textView12, textView14, textView20, textView21, textView24, textView25, textView27,
          textView28, textView29, textView7, binding_toolbarList);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
